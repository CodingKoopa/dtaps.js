'use strict';

require(`checkenv`).check();

const discord = require(`discord.js`);
const schedule = require(`node-schedule`);
const winston = require(`winston`);

winston.addColors({
  Error: `red`,
  Warn: `yellow`,
  Info: `green`,
  Verbose: `cyan`,
  Debug: `blue`,
  Silly: `magenta`
});
const logger = winston.createLogger(
  {
    levels: {
      Error: 0,
      Warn: 1,
      Info: 2,
      Verbose: 3,
      Debug: 4,
      Silly: 5
    },
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.simple()
    ),
    transports: [new winston.transports.Console({level: `Silly`})]
  });

logger.Info(`dtaps.js ${require(`./package.json`).version} starting up.`);

logger.Info(`Initializing Node.js event handlers.`);
process.on(`unhandledRejection`, error =>
{
  logger.Error(`Unhandled rejection: "${error.message}"`);
});
process.on(`uncaughtException`, error =>
{
  logger.Error(`Uncaught exception: "${error.message}"`);
  process.exit(-1);
});

logger.Info(`Initializing constants.`);
const discord_token = process.env.DTJS_TOKEN;
const primary_guild_id = process.env.DTJS_PRIMARY_GUILD_ID;
const blaze_channel_id = process.env.DTJS_BLAZE_CHANNEL_ID;
const talk_channel_id = process.env.DTJS_TALK_CHANNEL_ID;

const days = [`Sunday`, `Monday`, `Tuesday`, `Wednesday`, `Thursday`, `Friday`, `Saturday`];
const emotions = [`excited`, `tired`, `happy`, `sad`];

const greetings = [
  `Ah, welcome to $DAY!$LATER_EMOTION`,
  `Good morning!$LATER_EMOTION`
];
const question_answers = [
  `Hmm, that's a complicated question. I would say so.`,
  `Yeah.`,
  `Not quite.`,
  `You bring up an interesting question. Analyzing the historical context of that time period, it \
seems not.`
];
const generic_responses = [
  `Hmm... Interesting.`,
  `Yeah, to some extent, $STRIPPED_MESSAGE.`,
  `You bring up a good point.`,
  `*brushes back hair with hand*`,
  `Yeah, $STRIPPED_MESSAGE is a good example of cultural diffusion.`,
  `I'll get back to you in a minute.$LATER_THINK`,
  `*props one foot up on chair*`,
  `*starts playing with tie*`,
  `Heh, corny Friday jokes.`,
  `We're going to read a couple of documents about $LAST_STRIPPED_MESSAGE, and then \
some $STRIPPED_MESSAGE at the end of the period.`,
  `You're going to create a thesis statement about $STRIPPED_MESSAGE.`,
  `*leans up against desk*`, [
    `Today we're going to $STRIPPED_MESSAGE a little bit.`,
    `We're going to $STRIPPED_MESSAGE a little bit today.`,
  ]
];

logger.Info(`Initializing globals.`);
let message_string = `Good memes.`;
let stripped_message_string = `bad memes`;
let last_message_string = `Ok memes.`;
let last_stripped_message_string = `anime`;

logger.Info(`Initializing Discord event handlers.`);
let initialized = false;
const client = new discord.Client();
let bot_user_id;
client.on(`ready`, () =>
{
  if (!initialized)
  {
    logger.Info(`Logged into Discord.`);

    logger.Info(`Initializing Discord globals.`);
    bot_user_id = client.user.id;

    logger.Info(`Scheduling Discord events.`);
    // The 5 second delay is necessary, or else the message will be sent too early.
    schedule.scheduleJob(`5 20 4,16 * * *`, () =>
    {
      logger.Debug(`Blaze event triggered.`);
      client.guilds.cache.get(primary_guild_id).channels.cache.get(blaze_channel_id).send(`***\
@everyone 420 BLESS UP :pray:***`);
    });

    client.user.setActivity(`Reading World Civilizations - The Global Experience (Fourth Edition)`);

    initialized = true;
  }
});

function GetRandomUserMention(channel)
{
  let user_id = ``;
  if (channel.members)
    user_id = channel.members.filter(member => !member.user.bot).random().id;
  else if (channel.recipients)
    user_id = channel.recipients.random().id;
  else if (channel.recipient)
    user_id = channel.recipient.id;
  else
    return user_id;

  return `<@${user_id}>, `;
}

function ReplyRandomQuote(message, array)
{
  // Descend the arrays until an argument has been found.
  while (Array.isArray(array))
    array = array[Math.floor(Math.random() * Math.floor(array.length))];

  // If the argument is a string, perform runtime string substitution.
  array = array.replace(`$MESSAGE`, message_string).replace(
    `$STRIPPED_MESSAGE`, stripped_message_string).replace(
    `$LAST_MESSAGE`, last_message_string).replace(
    `$LAST_STRIPPED_MESSAGE`, last_stripped_message_string).replace(
    `$DAY`, days[new Date().getDay()]);

  if (array.indexOf(`$LATER_EMOTION`) !== -1)
  {
    array = array.replace(`$LATER_EMOTION`, ``);
    schedule.scheduleJob(Date.now() + 2500, () =>
    {
      message.channel.send(`${GetRandomUserMention(message.channel)}Looking \
${emotions[Math.floor(Math.random() * Math.floor(emotions.length))]}!`);
    });
  }
  if (array.indexOf(`$LATER_THINK`) !== -1)
  {
    array = array.replace(`$LATER_THINK`, ``);
    schedule.scheduleJob(Date.now() + 60000, () => message.channel.send(`\
${GetRandomUserMention(message.channel)}What do you think?`));
  }

  message.reply(array);
}

// When a new message is recieved.
client.on(`message`, message =>
{
  if (/owokiss.*/gm.test(message.content))
  {
    message.reply(`Could you meet me after class, so we can talk about your essay?`);
    message.reply(`*brushes back hair with hand*`);
    message.reply(`*leans up against desk*`);
  }

  // Don't do anything for bot messages, our own messages, or empty messages. If in a server outside
  // of the dedicated talk channel, don't send a message unless being mentioned.
  if (message.author.bot || !message.content || (message.guild &&
    message.channel.id !== talk_channel_id) &&
    !message.mentions.members.has(bot_user_id))
    return;

  message_string = message.content;
  // Make the message lowercase so that the commands don't have to be case sensitive.
  stripped_message_string = message.content.toLowerCase().replace(/(\.|!|\?)/gm, ``);

  // If the user is saying hi.
  if (/\b(h(i|ey)+|h(e)+(l)+(o)+|greetings)\b/gm.test(stripped_message_string))
  {
    ReplyRandomQuote(message, greetings);
  }
  else if (stripped_message_string.length > 100)
  {
    message.reply(`Just one quick point, Ryan.`);
  }
  else if (message_string === message_string.toUpperCase())
  {
    message.reply(`Heh, Mr. Braun is a little bit fired up today.`);
  }
  else if (message_string.indexOf(`?`) !== -1)
  {
    ReplyRandomQuote(message, question_answers);
  }
  // If none of the filters were triggered.
  else
  {
    ReplyRandomQuote(message, generic_responses);
  }
  last_message_string = message.content;
  last_stripped_message_string = stripped_message_string;
});

logger.Info(`Logging in.`);
try
{
  client.login(discord_token);
}
catch(e)
{
  logger.Error(`Failed to log into Discord: ${e}.`);
  process.exit(-2);
}
